package RouteBots;/*
 *Author:jfaldanam
 *
 */
import RouteBots.robotPositions;
import robocode.control.*;
import robocode.control.events.*;

import java.util.Random;
import java.util.Set;


public class MazeGenerator {


    /*
        MUST BE IN SYNC WITH RouteBot.java !!!!!
        Constants to tweak how the application works
        pathRobocode must point to your own robocode installation path
        seed to pass to the robot so it knows the layout of the map
        xSize and ySize must be multiples of 64 (size of each tile graphically)
    */
    static final long seed = 1234567890;
    static final int xSize=1728; //27 Tiles Wide
    static final int ySize=896; //14 Tiles Height

    //Uncomment your pathCode

    //static final String pathRobocode = "/home/cristian/robocode/"; //Thecristian1409
    static final String pathRobocode = "/opt/robocode/"; //jfaldanam
    //static final String pathRobocode = "C://robocode"; //javi





    public static void main(String[] args) {

        // Create the RobocodeEngine
        RobocodeEngine engine = new RobocodeEngine(new java.io.File(pathRobocode));


        // Show the Robocode battle view
        engine.setVisible(true);

        // Create the battlefield, multiples of 64
        BattlefieldSpecification battlefield = new  BattlefieldSpecification(xSize, ySize);

        // Setup battle parameters
        int numberOfRounds = 5;
        long inactivityTime = 10000000;
        double gunCoolingRate = 1.0;
        int sentryBorderSize = 50;
        boolean hideEnemyNames = true;

        //Create as many sittingDucks as 33% of all tiles
        int NumObstacles = (int)(((xSize/64)*(ySize/64))*0.33);


        //Object to generate positions for all robots/goal
        robotPositions positioning = new robotPositions(xSize,ySize,seed);


        /*
         * Create obstacles and place them at random following a seed so that no pair of obstacles
         * are at the same position
         */

        //Get all information from the robots on the robocode installation
        RobotSpecification[] modelRobots = engine.getLocalRepository("sample.SittingDuck,RouteBots.RouteBot*");

        //Create an array to hold the information of all bots
        RobotSpecification[] existingRobots = new RobotSpecification[NumObstacles+1];

        //Creates an array to hold all positions of all bots
        RobotSetup[] robotSetups = new RobotSetup[NumObstacles+1];
        double xPos;
        double yPos;

        //Generates position and saves all info for the sittingDucks bots
        for(int NdxObstacle=0;NdxObstacle<NumObstacles;NdxObstacle++) {

            positioning.generate();
            xPos = positioning.getxUnused();
            yPos = positioning.getyUnused();

            existingRobots[NdxObstacle]=modelRobots[0];
            robotSetups[NdxObstacle]=new RobotSetup(xPos, yPos,0.0);
        }
        /*
         * Create the agent and place it in a random position without obstacle
         */
        existingRobots[NumObstacles]=modelRobots[1];
        positioning.generate();

        xPos = positioning.getxUnused();
        yPos = positioning.getyUnused();
        robotSetups[NumObstacles]=new RobotSetup(xPos, yPos,0.0);

        /* Create and run the battle */
        BattleSpecification battleSpec = new BattleSpecification(battlefield,
                        numberOfRounds,
                        inactivityTime,
                        gunCoolingRate,
                        sentryBorderSize,
                        hideEnemyNames,
                        existingRobots,
                        robotSetups);

        // Run our specified battle and let it run till it is over
        engine.runBattle(battleSpec, true); // waits till the battle finishes

        // Cleanup our RobocodeEngine
        engine.close();

        // Make sure that the Java VM is shut down properly
        System.exit(0);
    }
}