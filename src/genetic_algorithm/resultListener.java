package genetic_algorithm;

import robocode.BattleResults;
import robocode.control.events.*;

import java.io.*;


public class resultListener implements IBattleListener {
    @Override
    public void onBattleStarted(BattleStartedEvent battleStartedEvent) {


    }

    @Override
    public void onBattleFinished(BattleFinishedEvent battleFinishedEvent) {

    }

    @Override
    public void onBattleCompleted(BattleCompletedEvent battleCompletedEvent) {

        BattleResults[] result = battleCompletedEvent.getIndexedResults();
        double[] score = {result[0].getScore(),result[1].getScore()}; // 0 enemy - 1 our bot
        PrintWriter writer = null;
        try {
            writer = new PrintWriter("communication.txt");
            writer.println(score[0]);
            writer.println(score[1]);
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onBattlePaused(BattlePausedEvent battlePausedEvent) {

    }

    @Override
    public void onBattleResumed(BattleResumedEvent battleResumedEvent) {

    }

    @Override
    public void onRoundStarted(RoundStartedEvent roundStartedEvent) {

    }

    @Override
    public void onRoundEnded(RoundEndedEvent roundEndedEvent) {

    }

    @Override
    public void onTurnStarted(TurnStartedEvent turnStartedEvent) {

    }

    @Override
    public void onTurnEnded(TurnEndedEvent turnEndedEvent) {

    }

    @Override
    public void onBattleMessage(BattleMessageEvent battleMessageEvent) {

    }

    @Override
    public void onBattleError(BattleErrorEvent battleErrorEvent) {

    }
}
