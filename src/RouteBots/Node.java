package RouteBots;

public class Node implements Comparable<Node> {

    public int x;
    public int y;
    public int g;
    public int h;
    public Node parent;
    
    
    public Node(int xx, int yy, int gValue, int hValue, Node p){
        x = xx;
        y = yy;
        g = gValue;
        h = hValue;
        parent = p;    
    }

    
	@Override
	public int compareTo(Node o) {
		
		return Integer.compare(g + h, o.g + o.h);
	}

    

}