#!/usr/bin/python
import numpy as num
import matplotlib.pyplot as plt
from matplotlib import interactive
import sys

popu_size = int(sys.argv[1])

with open('score.txt') as f:
    floats = list(map(float, f))

i = 0
lstMax = []
lstMean = []
while i < len(floats):
    maxIt = max(floats[i:i+popu_size])
    meanIt = num.mean(floats[i:i+popu_size])
    if len(floats[i:i+popu_size]) == popu_size:
        lstMax.append(maxIt)
        lstMean.append(meanIt)
    i = i + popu_size

fig, ax = plt.subplots( nrows=1, ncols=2 )  # create figure & 1 axis
fig.canvas.set_window_title("Graphs")
ax[0].plot(lstMax)
ax[0].set_title('Max')
ax[1].plot(lstMean)
ax[1].set_title('Mean')
fig.savefig('graph.png')   # save the figure to file
plt.show()
plt.close(fig)
