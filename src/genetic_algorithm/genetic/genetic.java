package genetic_algorithm.genetic;


import org.jgap.*;
import org.jgap.impl.DefaultConfiguration;
import org.jgap.impl.DoubleGene;
import org.jgap.impl.IntegerGene;

import java.io.IOException;
import java.io.PrintWriter;

import static java.lang.System.exit;

public class genetic {

    private static final int MAX_EVOLUTIONS = 200;
    private static final int POPULATION_SIZE = 20;

    public genetic() {

    }

    public static void main(String[] args) throws Exception {
        new PrintWriter("score.txt").close();
        Configuration conf = new DefaultConfiguration();
        FitnessFunction fitness = new SuperTrackerFitnessFunction();
        try {
            conf.setFitnessFunction(fitness);
        } catch (InvalidConfigurationException e) {
            throw new Exception("Wrong invalid exception");
        }

        Gene[] parameters = new Gene[] {
                new IntegerGene(conf,100,200),   //Distance limit 150
                new DoubleGene(conf,0.0,0.0),       //Probability change speed 0.1
                new DoubleGene(conf, 8, 12),   //Range of speed 12
                new DoubleGene(conf,8,12)      //Minimum Speed 12
        };
        IChromosome chromo = new Chromosome(conf,parameters);
        conf.setPreservFittestIndividual(true);
        conf.setSampleChromosome(chromo);
        conf.setPopulationSize(POPULATION_SIZE);

        Genotype population = Genotype.randomInitialGenotype(conf);

       for (int i = 0; i < MAX_EVOLUTIONS; i++) {
                population.evolve();
       }




        System.out.println("Finished");
        try {
            Process p1 = Runtime.getRuntime().exec("python ./src/genetic_algorithm/generate_graph.py "+ POPULATION_SIZE);
        } catch(IOException e) {
            e.printStackTrace();
        }
        exit(0);

    }
}
