package genetic_algorithm;
import robocode.*;
import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;


/**
 * SuperTracker - a Super Sample Robot by CrazyBassoonist based on the robot Tracker by Mathew Nelson and maintained by Flemming N. Larsen
 * <p/>
 * Locks onto a robot, moves close, fires when close.
 */
public class SuperTracker extends AdvancedRobot {
    int moveDirection = 1;//which way to move
    //
    int distanceLimit = 150;
    double probChangeSpeed = 0.1;
    double rangeSpeed = 12;
    double minSpeed = 12;

    boolean firstRun = true;
    /**
     * run:  Tracker's main run function
     */
    public void run() {
        if (firstRun) readParameters();
        setAdjustRadarForRobotTurn(true);//keep the radar still while we turn
        setBodyColor(new Color(255, 105, 180));
        setGunColor(new Color(255, 255, 0));
        setRadarColor(new Color(0, 0, 0));
        setScanColor(Color.white);
        setBulletColor(new Color(255, 105, 180));
        setAdjustGunForRobotTurn(true); // Keep the gun still when we turn
        turnRadarRightRadians(Double.POSITIVE_INFINITY);//keep turning radar right
    }

    private void readParameters() {
        try {
            Scanner input = new Scanner(new File("/home/jfaldanam/Software/Robocoded/communication.txt"));
            distanceLimit = input.nextInt();
            probChangeSpeed = input.nextDouble();
            rangeSpeed = input.nextDouble();
            minSpeed = input.nextDouble();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        firstRun = false;
    }

    /**
     * onScannedRobot:  Here's the good stuff
     */
    public void onScannedRobot(ScannedRobotEvent e) {
        double absBearing=e.getBearingRadians()+getHeadingRadians();//enemies absolute bearing
        double latVel=e.getVelocity() * Math.sin(e.getHeadingRadians() -absBearing);//enemies later velocity
        double gunTurnAmt;//amount to turn our gun
        setTurnRadarLeftRadians(getRadarTurnRemainingRadians());//lock on the radar
        if(Math.random()>1-probChangeSpeed){
            setMaxVelocity((rangeSpeed*Math.random())+minSpeed);//randomly change speed
        }
        if (e.getDistance() > distanceLimit) {//if distance is greater than 150
            gunTurnAmt = robocode.util.Utils.normalRelativeAngle(absBearing- getGunHeadingRadians()+latVel/22);//amount to turn our gun, lead just a little bit
            setTurnGunRightRadians(gunTurnAmt); //turn our gun
            setTurnRightRadians(robocode.util.Utils.normalRelativeAngle(absBearing-getHeadingRadians()+latVel/getVelocity()));//drive towards the enemies predicted future location
            setAhead((e.getDistance() - 140)*moveDirection);//move forward
            setFire(3);//fire
        }
        else{//if we are close enough...
            gunTurnAmt = robocode.util.Utils.normalRelativeAngle(absBearing- getGunHeadingRadians()+latVel/15);//amount to turn our gun, lead just a little bit
            setTurnGunRightRadians(gunTurnAmt);//turn our gun
            setTurnLeft(-90-e.getBearing()); //turn perpendicular to the enemy
            setAhead((e.getDistance() - 140)*moveDirection);//move forward
            setFire(3);//fire
        }
    }

    public void onHitWall(HitWallEvent e){
        moveDirection=-moveDirection;//reverse direction upon hitting a wall
    }

    /**
     * onWin:  Do a victory dance
     */
    public void onWin(WinEvent e) {
        for (int i = 0; i < 50; i++) {
            turnRight(30);
            turnLeft(30);
        }
    }

}