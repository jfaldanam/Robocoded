package genetic_algorithm;/*
 *Author:jfaldanam
 *
 */
import RouteBots.robotPositions;
import robocode.control.*;
import robocode.control.events.*;



public class Battle_gen_alg {


    /*
        MUST BE IN SYNC WITH RouteBot.java !!!!!
        Constants to tweak how the application works
        pathRobocode must point to your own robocode installation path
        seed to pass to the robot so it knows the layout of the map
        xSize and ySize must be multiples of 64 (size of each tile graphically)
    */

    final int xSize=512; //27 Tiles Wide
    final int ySize=512; //14 Tiles Height

    //Uncomment your pathCode

    //final String pathRobocode = "/home/cristian/robocode/"; //Thecristian1409
    final String pathRobocode = "/opt/robocode/"; //jfaldanam
    //final String pathRobocode = "C://robocode"; //javi





    public void runBattle() {

        // Create the RobocodeEngine
        RobocodeEngine engine = new RobocodeEngine(new java.io.File(pathRobocode));


        // Show the Robocode battle view
        engine.setVisible(false);

        // Create the battlefield, multiples of 64
        BattlefieldSpecification battlefield = new  BattlefieldSpecification(xSize, ySize);

        // Setup battle parameters
        int numberOfRounds = 100; //10 maybe
        long inactivityTime = 10000000;
        double gunCoolingRate = 1.0;
        int sentryBorderSize = 50;
        boolean hideEnemyNames = false;



        /*
         * Create obstacles and place them at random following a seed so that no pair of obstacles
         * are at the same position
         */

        //Get all information from the robots on the robocode installation
        RobotSpecification[] modelRobots = engine.getLocalRepository("genetic_algorithm.SuperRamFire*,genetic_algorithm.SuperTracker*");

        //Create an array to hold the information of all bots
        RobotSpecification[] existingRobots = new RobotSpecification[2];

        //Creates an array to hold all positions of all bots
        RobotSetup[] robotSetups = new RobotSetup[2];
        double xPos;
        double yPos;

        /*
         * Create the agent and place it in a random position without obstacle
         */
        existingRobots[0]=modelRobots[0];

        xPos = 32;
        yPos = 256;
        robotSetups[0] = new RobotSetup(xPos, yPos,90.0);
        /*
         * Create the agent and place it in a random position without obstacle
         */
        existingRobots[1]=modelRobots[1];

        xPos = 480;
        yPos = 256;
        robotSetups[1] = new RobotSetup(xPos, yPos,270.0);

        /* Create and run the battle */
        BattleSpecification battleSpec = new BattleSpecification(battlefield,
                numberOfRounds,
                inactivityTime,
                gunCoolingRate,
                sentryBorderSize,
                hideEnemyNames,
                existingRobots,
                robotSetups);


        IBattleListener resultListener = new resultListener();

        engine.addBattleListener(resultListener);

        // Run our specified battle and let it run till it is over
        engine.runBattle(battleSpec, true); // waits till the battle finishes

        // Cleanup our RobocodeEngine
        engine.close();
    }
}