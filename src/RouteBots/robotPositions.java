/*
 *Author:jfaldanam
 *
 */
package RouteBots;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class robotPositions {
    // xSize and ySize are the size in pixels of the battleground must be multiples of 64
    private int xSize;
    private int ySize;
    //seed for the Random object
    private final long seed;
    //xUnused and yUnused have the last data generated.
    //Will have -1 if no data is available
    private double xUnused;
    private double yUnused;
    //Set with all coordinated ocuped
    //Format: "xCoord yCoord"
    private Set<String> filled;
    //Random object
    private Random rng;


    //Constructor
    public robotPositions(int xSize, int ySize, long seed) {
        this.xSize = xSize;
        this.ySize = ySize;
        this.seed = seed;
        filled = new HashSet<String>();
        rng = new Random(seed);
    }



    /*
       This method generates a new pair of positions to place all bots (sittingDucks and our robot)
       Should be use to create the Goal positon for the bot too
       Output in xUnused and yUnused
    */
    public void generate() {
        xUnused = normalize((double)rng.nextInt(xSize));
        yUnused = normalize((double)rng.nextInt(ySize));

        while(filled.contains(""+xUnused+" "+yUnused)) {
            xUnused = normalize((double)rng.nextInt(xSize));
            yUnused = normalize((double)rng.nextInt(ySize));
        }
        filled.add(""+xUnused+" "+yUnused);

    }

    /*
        Normalize the coordinate by returning the largest multiple of 64 that is smaller that the received value,
        adds 32 to center it graphically
    */
    private double normalize(double num) {
        return (((int)num / 64) * 64)+32;
    }

    /*
        Returns the last unused X value, and sets it to -1
        Returns -1 if no new number has been generated
    */
    public double getxUnused() {
        double temp = xUnused;
        xUnused = -1;
        return temp;
    }

    /*
        Returns the last unused Y value, and sets it to -1
        Returns -1 if no new number has been generated
    */
    public double getyUnused() {
        double temp = yUnused;
        yUnused = -1;
        return temp;
    }

    public Set<String> getOcupied() {
        return filled;
    }
}
