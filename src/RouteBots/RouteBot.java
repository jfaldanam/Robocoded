package RouteBots;
import robocode.Robot;
import RouteBots.robotPositions;


import java.util.PriorityQueue;


import static java.lang.Math.*;

public class RouteBot extends Robot{

    /* MUST BE IN SYNC WITH RouteBots.MazeGenerator.java !!!!!
    Constants to tweak how the application works.
    seed to pass to the robot so it knows the layout of the map
    xSize and ySize must be multiples of 64 (size of each tile graphically)
*/
    private static final long seed = 1234567890;
    private static final int xSize=1728; //27 Tiles Wide
    private static final int ySize=896; //14 Tiles Height
    private int table[][] = new int[xSize/64][ySize/64];
    private int xGoal, yGoal, xCurrent, yCurrent, g;
    private Node goalNode;

//The valor 1 for our matrix means that there is a SittingDuck, 0 means that the block is free, 2 means our robot is there, and 3 is the goal
    
    
   public void run() {
    	  
	   generateScenario();
	   
	   astar();
	   
	   int[] movements = reconstructPath();
	   	   
	   for (int i = 0; i < movements.length; i++) {
		       	  
	  	   if (movements[i] == 0) {
    		   
	  		   int heading = (int) getHeading();
    		   
	  		   switch(heading) {
	  		   		
	  		   	case 0:		ahead(64);
	  		   				break;
	  		   	case 90:	turnLeft(90);
	  		   				ahead(64);
	  		   				break;
	  		   	case 180:	turnLeft(180);
			   				ahead(64);
			   				break;
	  		   	case 270:	turnRight(90);
			   				ahead(64);
			   				break;	  		  
	  		   }
	  		   
	  		  
    	   } else if (movements[i] == 3) {
    		 
    		   int heading = (int) getHeading();
    		   
	  		   switch(heading) {
	  		   		
	  		   	case 90:	ahead(64);
	  		   				break;
	  		   	case 180:	turnLeft(90);
	  		   				ahead(64);
	  		   				break;
	  		   	case 270:	turnLeft(180);
			   				ahead(64);
			   				break;
	  		   	case 0:		turnRight(90);
			   				ahead(64);
			   				break;	  		  
	  		   }
    	   
    	   } else if (movements[i] == 6) {
    		  
    		   int heading = (int) getHeading();
    		   
	  		   switch(heading) {
	  		   		
	  		   	case 180:	ahead(64);
	  		   				break;
	  		   	case 270:	turnLeft(90);
	  		   				ahead(64);
	  		   				break;
	  		   	case 0:		turnLeft(180);
			   				ahead(64);
			   				break;
	  		   	case 90:	turnRight(90);
			   				ahead(64);
			   				break;	  		  
	  		   }
    	  
    	   } else if (movements[i] == 9) {
    		 
    		   int heading = (int) getHeading();
    		   
	  		   switch(heading) {
	  		   		
	  		   	case 270:	ahead(64);
	  		   				break;
	  		   	case 0:		turnLeft(90);
	  		   				ahead(64);
	  		   				break;
	  		   	case 90:	turnLeft(180);
			   				ahead(64);
			   				break;
	  		   	case 180:	turnRight(90);
			   				ahead(64);
			   				break;	  		  
	  		   }
    	   }
       }
	   
	   
    }


    private int manhattan(int xGoal, int yGoal, int xPos, int yPos){

        int distance = 0;

        distance = abs(xGoal - xPos) + abs(yGoal - yPos);

        return distance;
    }

    
    private void generateScenario() {
    	
    	int NumObstacles = (int)(((xSize/64)*(ySize/64))*0.33);
       
        
        robotPositions positions = new robotPositions(xSize, ySize, seed);
        
        //Generates positions for SittingDucks
        for(int i = 0; i < NumObstacles; i++){
        	
        	
            positions.generate();
           
            
            int x = (int) positions.getxUnused()/64;
            int y = (int) positions.getyUnused()/64;
                 
            table[x][y] = 1;
            
        }
        

        //Generates position for our robot
        positions.generate();
        xCurrent = (int)positions.getxUnused()/64;
        yCurrent = (int)positions.getyUnused()/64;
        table[xCurrent][yCurrent] = 2;

        //Generates position for the goal
        positions.generate();
        xGoal = (int)positions.getxUnused()/64;
        yGoal = (int)positions.getyUnused()/64;

        table[xGoal][yGoal] = 3;

        
    	
    }
    
    

    private void astar(){

        //Up to now, we have the closedset(represented with 1s) with all the SittingDucks,
        //the position of the goal, the freetovisitSet(represented with 0s),
        //and the current position of the robot, now we check its adjacent blocks

    	
        g = 0;
    	
        PriorityQueue<Node> openset = new PriorityQueue<Node>();
        
        int h = manhattan(xGoal, yGoal, xCurrent, yCurrent);
        
        Node start = new Node(xCurrent, yCurrent, g, h, null);

        fillOpenset(openset, start);
        
        boolean gg = false;

        while(!openset.isEmpty() && !gg){

            Node parentNode = openset.poll();
            
            g = parentNode.g + 1;
            xCurrent = parentNode.x;
            yCurrent = parentNode.y;
            
            table[xCurrent][yCurrent] = 1;
            
            if(xCurrent == xGoal && yCurrent == yGoal){

                gg = true;
                goalNode = parentNode;
            }

            fillOpenset(openset, parentNode);

        }                       

    }



	private void fillOpenset(PriorityQueue<Node> p, Node n){
		
		if (xCurrent < xSize/64 -1) {
			
			if( table[xCurrent+1][yCurrent] != 1 ){
				
				int h = manhattan(xGoal, yGoal, xCurrent+1, yCurrent);

				p.add(new Node(xCurrent+1, yCurrent, g, h, n));
			}
		}
		
		
		if ( xCurrent > 0) {
			
			if ( table[xCurrent-1][yCurrent] != 1 ){
				
				int h = manhattan(xGoal, yGoal, xCurrent-1, yCurrent);
				
				p.add(new Node (xCurrent-1, yCurrent, g, h, n));				
            
			}
		}
		
		if ( yCurrent < ySize/64 -1 ) {
			
			
			if (table[xCurrent][yCurrent+1] != 1 ){

	            int h = manhattan(xGoal, yGoal, xCurrent, yCurrent+1);
				
				p.add(new Node(xCurrent, yCurrent+1, g, h, n));
	            
	        }
		}
		
		if ( yCurrent > 0) {
						
			 if (table[xCurrent][yCurrent-1] != 1 ){

				 int h = manhattan(xGoal, yGoal, xCurrent, yCurrent-1);
				 
				 p.add(new Node(xCurrent, yCurrent-1, g, h, n));

		     }	
		}

    }
	
	
	private int[] reconstructPath() {
		
		Node child = goalNode;
		
		int[] movements = new int[goalNode.g + 1]; // in this particular case g is the number of movements
		
		while (child.parent != null) {
			
			if (child.x == child.parent.x + 1 ) {
				
				movements[child.g] = 3;
			
			} else if (child.x == child.parent.x - 1) {
				
				movements[child.g] = 9;
			
			} else if (child.y == child.parent.y + 1) {
				
				movements[child.g] = 0;
				
			} else {
				
				movements[child.g] = 6;
			}			
				
				
			child = child.parent;	
			
		}
		
		return movements;
	}
    

}
 
 