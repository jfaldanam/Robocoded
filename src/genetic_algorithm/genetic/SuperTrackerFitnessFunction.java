package genetic_algorithm.genetic;

import genetic_algorithm.Battle_gen_alg;
import org.jgap.FitnessFunction;
import org.jgap.IChromosome;

import java.io.*;
import java.util.Scanner;

public class SuperTrackerFitnessFunction extends FitnessFunction {
    double scoreEnemy, scoreFriendly;

    public SuperTrackerFitnessFunction() {

    }


    @Override
    protected double evaluate(IChromosome iChromosome) {
        double fitnessResult;
        PrintWriter writer;
        try {
            writer = new PrintWriter("communication.txt");
            writer.println(iChromosome.getGene(0).toString().split("=")[1]);
            writer.println(iChromosome.getGene(1).toString().split("=")[1]);
            writer.println(iChromosome.getGene(2).toString().split("=")[1]);
            writer.println(iChromosome.getGene(3).toString().split("=")[1]);
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


        Battle_gen_alg battle = new Battle_gen_alg();
        battle.runBattle();




        try {
            Scanner input = new Scanner(new File("communication.txt"));
            scoreEnemy = input.nextDouble(); // add score
            scoreFriendly = input.nextDouble();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        fitnessResult = scoreFriendly-scoreEnemy + 10000; //Calculate fitness value

        try { //Print fitness value for informational purposes
            writer = new PrintWriter(new FileOutputStream(
                    new File("score.txt"),
                    true /* append = true */));
            writer.println(fitnessResult);
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return fitnessResult;
    }


}
